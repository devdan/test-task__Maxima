var slideNow = 1;
var slideCount = $(".slider__list").children().length;
var translateWidth = 0;
var slideInterval = 5000;

function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('.slider__list').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('.slider').width() * (slideNow);
        $('.slider__list').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow++;
    }
}

function prevSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('.slider').width() * (slideCount - 1);
        $('.slider__list').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('.slider').width() * (slideNow - 2);
        $('.slider__list').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow--;
    }
}


$(document).ready(function () {

    var switchInterval = setInterval(nextSlide, slideInterval);

    $('.slider').hover(function(){
        clearInterval(switchInterval);
    },function() {
            switchInterval = setInterval(nextSlide, slideInterval);
    });

    $('.slider__toggle--next').on("click", function() {
        nextSlide();
    });

    $('.slider__toggle--prev').on("click", function() {
        prevSlide();
    });
});


