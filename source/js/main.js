var menuBtn = document.querySelector(".page-header__toggle"),

    siteNav = document.querySelector(".site-list");

    siteNav.classList.remove("site-list--nojs");
    menuBtn.classList.remove("page-header__toggle--nojs");

menuBtn.addEventListener("click", function(evt) {
  evt.preventDefault();

  if (siteNav.classList.contains("site-list--closed")) {
    siteNav.classList.remove("site-list--closed");
    siteNav.classList.add("site-list--opened");
  } else {
    siteNav.classList.add("site-list--closed");
    siteNav.classList.remove("site-list--opened");
  }
});

menuBtn.addEventListener("click", function() {
  if(menuBtn.classList.contains("page-header__toggle--opened")) {
    menuBtn.classList.remove("page-header__toggle--opened");
    menuBtn.classList.add("page-header__toggle--closed")
  } else {
    menuBtn.classList.remove("page-header__toggle--closed")
    menuBtn.classList.add("page-header__toggle--opened");
  }
});
